<!DOCTYPE html>
<html>
<head>
        <title>Result!</title>
</head>

<body>
<?php
function add($x, $y){
        return $x+$y;
}

function sub($x, $y){
        return $x-$y;
}

function multi($x, $y){
        return $x*$y;
}

function div($x, $y){
        return $x/$y;
}
?>

The result is
<?php
if ($_POST["means"]=="+") {
        echo add($_POST["d1"],$_POST["d2"]);
} elseif ($_POST["means"]=="-"){
        echo sub($_POST["d1"],$_POST["d2"]);
} elseif ($_POST["means"]=="*"){
        echo multi($_POST["d1"],$_POST["d2"]);
} elseif ($_POST["means"]=="/"){
        echo div($_POST["d1"],$_POST["d2"]);
} 
?>.

</body>

</html>